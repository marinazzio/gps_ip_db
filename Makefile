clean:
	docker rm gps_ip_db || true

clean_test:
	docker rm gps_ip_db_test || true

image: clean
	docker build --build-arg master_key=$(cat config/master.key) -t gps_ip_db -f Dockerfile .

test_image: clean_test
	docker build --build-arg master_key=$(cat config/master.key) -t gps_ip_db_test -f Dockerfile.dev .

test: clean_test
	docker run -it --name=gps_ip_db_test gps_ip_db_test bundle exec rails spec

run: clean
	docker run -it -p 3000:3000 --name=gps_ip_db gps_ip_db
