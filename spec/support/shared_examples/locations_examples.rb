RSpec.shared_examples 'creating a new location' do
  it 'creates a new location' do
    create_location

    expect(response).to have_http_status(:created)
  end
end

RSpec.shared_examples 'resource already exists' do
  it 'returns conflict status code' do
    create_location

    expect(response).to have_http_status(:conflict)
  end
end

RSpec.shared_examples 'resource is missing' do
  it 'returns not found error' do
    delete_location

    expect(response).to have_http_status(:not_found)
  end

  it 'does non delete location and url alias' do
    expect { delete_location }
      .to not_change(Location, :count)
      .and not_change(LocationAlias, :count)
  end
end

RSpec.shared_examples 'successful filtering of completed location' do
  it 'returns resource with coordinates' do
    find_location

    aggregate_failures 'response format' do
      expect(response).to have_http_status(:ok)
      expect(response.content_type).to start_with('application/json')

      expect(response.parsed_body).to be_an(Array)
      expect(response.parsed_body.size).to eq(1)

      loc = response.parsed_body.first[:model]

      expect(loc[:status]).to eq('success')
      expect(loc[:ip]).to eq(ip)
      expect(loc[:latitude]).not_to be_empty
      expect(loc[:longitude]).not_to be_empty
    end
  end
end

RSpec.shared_examples 'successful filtering of pending location' do
  it 'returns resource without coordinates' do
    find_location

    aggregate_failures 'response format' do
      expect(response).to have_http_status(:ok)
      expect(response.content_type).to start_with('application/json')

      expect(response.parsed_body).to be_an(Array)
      expect(response.parsed_body.size).to eq(1)

      loc = response.parsed_body.first[:model]

      expect(loc[:status]).to eq('pending')
      expect(loc[:ip]).to eq(ip)
      expect(loc[:latitude]).to be_blank
      expect(loc[:longitude]).to be_blank
    end
  end
end
