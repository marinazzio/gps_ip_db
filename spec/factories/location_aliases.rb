FactoryBot.define do
  factory :location_alias do
    name { Faker::Internet.domain_name }
  end
end
