FactoryBot.define do
  factory :location do
    transient do
      ipv6? { false }
      url { Faker::Internet.domain_name }
    end

    ip do
      ipv6? ? Faker::Internet.ip_v6_address : Faker::Internet.public_ip_v4_address
    end

    status { 'pending' }

    trait :with_url do
      after(:create) do |location, evaluator|
        create(:location_alias, name: evaluator.url, location:)
      end
    end

    trait :success do
      status { 'success' }
      latitude { Faker::Number.between(from: -90.0, to: 90.0).round(6) }
      longitude { Faker::Number.between(from: -90.0, to: 90.0).round(6) }
    end

    trait :pending do
      status { 'pending' }
    end
  end
end
