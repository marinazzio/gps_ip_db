require 'rails_helper'

RSpec.describe 'Locations' do
  define_negated_matcher :not_change, :change

  let(:headers) do
    {
      'Accept' => 'application/json',
      'Content-Type' => 'application/json'
    }
  end

  describe '#create' do
    subject(:create_location) { post('/locations', params:, as: :json, headers:) }

    let(:params) do
      {
        data: {
          type: 'locations',
          attributes:
        }
      }
    end

    context 'with valid ip' do
      let(:attributes) { { ip: '212.123.21.32' } }

      it_behaves_like 'creating a new location'
    end

    context 'with valid url' do
      let(:attributes) do
        { url: 'example.com' }
      end

      it_behaves_like 'creating a new location'
    end

    context 'with invalid params' do
      let(:attributes) { '' }

      it 'returns error status code' do
        create_location

        expect(response).to have_http_status(:bad_request)
      end
    end

    context 'with existing ip' do
      let(:ip) { Faker::Internet.public_ip_v4_address }

      let(:attributes) { { ip: } }

      before { create(:location, ip:) }

      it_behaves_like 'resource already exists'
    end

    context 'with existing url' do
      let(:url) { 'some.example.com' }

      let(:attributes) { { url: } }

      before { create(:location, :with_url, url:) }

      it_behaves_like 'resource already exists'
    end
  end

  describe '#index' do
    context 'with filtering by ip' do
      subject(:find_location) { get("/locations?ip=#{ip}", as: :json, headers:) }

      let(:ip) { Faker::Internet.public_ip_v4_address }

      context 'with completed location' do
        before { create(:location, :success, ip:) }

        it_behaves_like 'successful filtering of completed location'
      end

      context 'with pending location' do
        before { create(:location, :pending, ip:) }

        it_behaves_like 'successful filtering of pending location'
      end

      context 'with failed location' do
        let(:error_message) { Faker::Lorem.sentence }

        before { create(:location, ip:, status: error_message) }

        it 'returns resource with error message' do
          find_location

          aggregate_failures 'response format' do
            expect(response).to have_http_status(:ok)
            expect(response.content_type).to start_with('application/json')

            expect(response.parsed_body).to be_an(Array)
            expect(response.parsed_body.size).to eq(1)

            loc = response.parsed_body.first[:model]

            expect(loc[:status]).to eq(error_message)
            expect(loc[:ip]).to eq(ip)
            expect(loc[:latitude]).to be_blank
            expect(loc[:longitude]).to be_blank
          end
        end
      end
    end

    context 'with filtering by url' do
      subject(:find_location) { get("/locations?url=#{url}", as: :json, headers:) }

      let(:ip) { Faker::Internet.public_ip_v4_address }
      let(:url) { Faker::Internet.domain_name }

      context 'with completed location' do
        before { create(:location, :success, :with_url, ip:, url:) }

        it_behaves_like 'successful filtering of completed location'
      end
    end
  end

  describe '#destroy' do
    subject(:delete_location) { delete("/locations/#{deletion_param}", as: :json, headers:) }

    let(:ip) { Faker::Internet.public_ip_v4_address }
    let(:missing_ip) { Faker::Internet.public_ip_v4_address }
    let(:url) { Faker::Internet.domain_name }
    let(:missing_url) { Faker::Internet.domain_name }

    before { create(:location, :with_url, :success, ip:, url:) }

    context 'with deleting ip' do
      let(:deletion_param) { ip }

      it 'deletes location with specified IP' do
        expect { delete_location }
          .to change(Location, :count).by(-1)
          .and change(LocationAlias, :count).by(-1)
      end
    end

    context 'with deleting url' do
      let(:deletion_param) { url }

      it 'deletes url but spares IP' do
        expect { delete_location }
          .to not_change(Location, :count)
          .and change(LocationAlias, :count).by(-1)
      end
    end

    context 'with deleting missing ip' do
      let(:deletion_param) { missing_ip }

      it_behaves_like 'resource is missing'
    end

    context 'with deleting missing url' do
      let(:deletion_param) { missing_url }

      it_behaves_like 'resource is missing'
    end
  end
end
