require 'rails_helper'

PRIVATE_IPV4_ADDRESS_RANGES = [
  [10..10,   0..255,   0..255, 1..255],
  [172..172, 16..31,   0..255, 1..255],
  [192..192, 168..168, 0..255, 1..255]
].freeze

RSpec.describe Location do
  subject { described_class.new(ip:) }

  describe 'IP address validations' do
    context 'with IPv4' do
      context 'with correct public address' do
        let(:ip) { Faker::Internet.public_ip_v4_address }

        it { should be_valid }
      end

      context 'with loopback address' do
        let(:ip) { '127.0.0.1' }

        it { should be_invalid }
      end

      context 'with link local address' do
        let(:ip) { '169.254.123.215' }

        it { should be_invalid }
      end

      context 'with private address' do
        let(:ip) { PRIVATE_IPV4_ADDRESS_RANGES.sample.map { rand(_1) }.join('.') }

        it { should be_invalid }
      end
    end

    context 'with IPv6' do
      context 'with correct public address' do
        let(:ip) { Faker::Internet.ip_v6_address }

        it { should be_valid }
      end

      context 'with loopback address' do
        let(:ip) { '::1' }

        it { should be_invalid }
      end

      context 'with link local address' do
        let(:ip) { ['fe80', Faker::Internet.ip_v6_address.split(':').slice(1..)].join(':') }

        it { should be_invalid }
      end

      context 'with private address' do
        let(:ip) { ['fc00', Faker::Internet.ip_v6_address.split(':').slice(1..)].join(':') }

        it { should be_invalid }
      end
    end

    context 'with empty field' do
      let(:ip) { '' }

      it { should be_invalid }
    end
  end
end
