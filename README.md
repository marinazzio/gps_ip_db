## About

System components:
  * Rails API main application
  * SQLite3 database
  * Library [location_fetcher](https://gitlab.com/marinazzio/location_fetcher) to provide data from IPStack
  * Sidekiq background
  * Redis as sidekiq backend

## Prerequisites

You have to ensure `config/master.key` does exist. If it does not, regenerate a new one along with credentials:

```shell
  bin/rails credentials:edit --environment development
```

[location_fetcher](https://gitlab.com/marinazzio/location_fetcher) requires access key which could be retrieved on [IP Stack](https://ipstack.com/) site. Key value should be stored into `LOCATION_FETCHER_IPSTACK_API_KEY` environment variable.

## Launch

### Docker

  1. Ensure Docker is installed and running:
    ```shell
      docker -v
    ```
  2. Create `.env` file in the root directory of the project, put a line with access key:
    ```
      LOCATION_FETCHER_IPSTACK_API_KEY=here_is_actual_key_value
    ```
  3. Launch system with docker-compose:
    ```shell
      docker compose up --build
    ```
  4. If all is ok system should respond to `http://localhost:3000` with welcome screen

### Command line

  1. Ensure Ruby v3.2.2 is installed:
    ```shell
      ruby -v
    ```
  2. Install bundler:
    ```shell
      gem install bundler
    ```
  3. Install project dependencies:
    ```shell
      bundle install
    ```
  4. [Install](https://redis.io/docs/install/install-redis/) and run Redis server
  5. Initialize database:
    ```shell
      bundle exec rails db:create
      bundle exec rails db:migrate
      bundle exec rails db:seed
    ```
  6. Set `LOCATION_FETCHER_IPSTACK_API_KEY` value
  7. Install [foreman](https://github.com/ddollar/foreman):
    ```shell
      gem install foreman
    ```
  8. Launch system:
    ```shell
      foreman start
    ```
  9. If all is ok system should respond to `http://localhost:3000` with welcome screen

### Tests

Makefile uses Dockerfile.dev to build image for running specs:

```shell
  make test_image test
```

## Requests

Create and delete requests could be performed using `curl` or `Postman`, read could be done also from any browser.

### Create new record

By URL:

```shell
curl --location 'localhost:3000/locations' \
--header 'Content-Type: application/json' \
--header 'Content-Type: application/vnd.api+json' \
--data '{
    "data": {
        "type":"locations", 
        "attributes":{
            "url":"google.com"
        }
    }
}
'
```

By IP:

```shell
curl --location 'localhost:3000/locations' \
--header 'Content-Type: application/json' \
--header 'Content-Type: application/vnd.api+json' \
--data '{
    "data": {
        "type":"locations", 
        "attributes":{
            "ip":"1.1.1.1"
        }
    }
}
'
```

### Read by IP or URL

```shell
curl --location 'localhost:3000/locations?ip=1.1.1.1'
```

or

```shell
curl --location 'localhost:3000/locations?url=google.com'
```

### Delete URL or IP

```shell
curl --location --request DELETE 'localhost:3000/locations/1.1.1.1'
```

or 

```shell
curl --location --request DELETE 'localhost:3000/locations/google.com'
```

## Test data

App has built-in seeds which can be used to test API.

| IP     | URL    | Status |
| ------ | ------ | ------ |
| 48.95.144.16 | | pending |
| c676:be9b:80d0:2272:7589:ad9b:d332:4bb5 | | pending |
| | test-domain.example | pending |
| 198.197.217.97 | | success |
| 201.132.231.7 | | success |
| aa82:5416:34fc:380a:8ff:b5d9:33a0:60cb | | success |
| 4fc4:5254:ac5c:d7e6:e1cf:2a7e:605b:664f | schuster-cremin.example | success |
| 181.227.243.186 | reynolds.test | success |
| 172.214.191.93 | will.example | success |
| 83.154.213.220 | | error |
| 90.214.192.25 | | error |
| 130a:6b90:6e69:a947:2020:9813:a176:d8f0 | | error |

## Future plans

  1. Refactor URLs to support API versions
  2. Implement repository to incapsulate all requests to database
  3. Implement authentication and authorization using JWT
  