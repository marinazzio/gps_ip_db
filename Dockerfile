FROM ruby:3.2.2 AS build-env

ARG master_key

ENV RAILS_ENV=development
ENV RAILS_MASTER_KEY=$(master_key)

RUN apt-get update -qq && apt-get install --no-install-recommends -y build-essential libvips pkg-config

WORKDIR /app

COPY Gemfile Gemfile.lock /app/

RUN bundler_version=$(cat Gemfile.lock | grep -A 1 "BUNDLED WITH" | sed -n '2p' | tr -d [:blank:]) \
    && gem install bundler -v $bundler_version --no-document

RUN bundle install --retry=2 --no-cache

COPY . /app

FROM ruby:3.2.2

ENV RAILS_ENV=development
ENV REDIS_HOST=redis

RUN apt-get update && apt-get install --no-install-recommends -y curl libsqlite3-0 libvips

WORKDIR /app

COPY --from=build-env /usr/local/bundle /usr/local/bundle
COPY --from=build-env /app /app

RUN gem install foreman
RUN bundle install
RUN bundle exec rails db:migrate
RUN bundle exec rails db:seed

EXPOSE 3000

CMD [ "foreman", "start"]
