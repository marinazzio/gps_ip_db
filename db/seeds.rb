# This file should ensure the existence of records required to run the application in every environment (production,
# development, test). The code here should be idempotent so that it can be executed at any point in every environment.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Example:
#
#   ["Action", "Comedy", "Drama", "Horror"].each do |genre_name|
#     MovieGenre.find_or_create_by!(name: genre_name)
#   end

# pending
Location.create(ip: '48.95.144.16', status: 'pending')
Location.create(ip: 'c676:be9b:80d0:2272:7589:ad9b:d332:4bb5', status: 'pending')
no_ip = Location.create(status: 'pending')
no_ip.aliases.create(name: 'test-domain.example')

# completed IP-only
Location.create(ip: '198.197.217.97', status: 'success', longitude: '-18.448699', latitude: '86.806918')
Location.create(ip: '201.132.231.7', status: 'success', longitude: '-54.50011', latitude: '-12.103339')
Location.create(ip: 'aa82:5416:34fc:380a:8ff:b5d9:33a0:60cb', status: 'success', longitude: '14.859279', latitude: '-57.407792')

# completed with URL
with_url1 = Location.create(ip: '4fc4:5254:ac5c:d7e6:e1cf:2a7e:605b:664f', status: 'success', longitude: '40.91009', latitude: '22.200811')
with_url2 = Location.create(ip: '181.227.243.186', status: 'success', longitude: '48.727316', latitude: '50.825974')
with_url3 = Location.create(ip: '172.214.191.93', status: 'success', longitude: '70.499517', latitude: '-81.818015')
with_url1.aliases.create(name: 'schuster-cremin.example')
with_url2.aliases.create(name: 'reynolds.test')
with_url3.aliases.create(name: 'will.example')

# errors
Location.create(ip: '83.154.213.220', status: 'Connection timeout')
Location.create(ip: '90.214.192.25', status: 'Record not found')
Location.create(ip: '130a:6b90:6e69:a947:2020:9813:a176:d8f0', status: 'Access key not provided')
