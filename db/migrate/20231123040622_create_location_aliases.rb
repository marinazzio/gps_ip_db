class CreateLocationAliases < ActiveRecord::Migration[7.1]
  def change
    create_table :location_aliases do |t|
      t.belongs_to :location, index: true, foreign_key: true
      t.string :name

      t.timestamps
    end

    add_index :location_aliases, :name, unique: true
  end
end
