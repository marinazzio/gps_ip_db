class LocationsController < ApplicationController
  before_action :parse_create_params, only: [:create]
  before_action :parse_filter_params, only: [:index]
  before_action :parse_identifier, only: [:destroy]
  before_action :either_ip_or_url
  before_action :check_url, only: %i[create]
  before_action :check_ip, only: %i[create]

  attr_reader :ip, :url

  def index
    scope = Location
    scope = scope.where(ip:) if ip.present?
    scope = scope.joins(:aliases).where(aliases: { name: url }) if url.present?

    locations = scope.map { LocationResource.new(_1, context) }

    render json: locations, status: :ok
  end

  def create
    location = Location.new(status: 'pending')

    location.ip = ip if ip.present?

    raise EntityCreationError unless location.save

    location.aliases.create!(name: url) if url.present?
    fetch_geolocation(location)

    render json: LocationResource.new(location, context), status: :created
  end

  # it is better to refactor the error catching system and remove such kind of logic from this method
  def destroy
    resource = find_alias if url.present?
    resource = find_location if ip.present?

    if resource.present?
      resource.destroy

      head :no_content
    else
      render json: error_json('Resource not found'), status: :not_found
    end
  end

  private

  def find_alias
    LocationAlias.find_by name: url
  end

  def find_location
    Location.find_by ip:
  end

  def parse_identifier
    @ip = detect_ip
    @url = detect_url
  end

  def detect_ip
    @ip = IPAddr.new(params[:id]).to_s
  rescue IPAddr::InvalidAddressError
    nil
  end

  # it is a bad decision to put such a validation here
  # especially considering we already have this code in the locaction_fetcher gem
  def detect_url
    url_regex = /^(?!:\/\/)(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,}$/i
    @url = params[:id] if params[:id] =~ url_regex
  end

  def location_filter_params
    params.permit(:ip, :url)
  end

  def parse_filter_params
    @ip = location_filter_params[:ip]
    @url = location_filter_params[:url]
  end

  def location_create_params
    params.require(:data).require(:attributes).permit(:ip, :url)
  end

  def parse_create_params
    @ip = location_create_params[:ip]
    @url = location_create_params[:url]
  end

  def either_ip_or_url
    raise OnlyOneParameterAllowedError unless ip.present? ^ url.present?
  end

  def check_ip
    return if ip.blank?

    raise EntityAlreadyExistsError if Location.exists?(ip:)
  end

  def check_url
    return if url.blank?

    raise EntityAlreadyExistsError if LocationAlias.exists?(name: url)
  end

  def fetch_geolocation(location)
    FetchFromUplinkJob.perform_async(location.id)
  end
end
