class ApplicationController < ActionController::API
  include JSONAPI::ActsAsResourceController

  OnlyOneParameterAllowedError = Class.new(StandardError)
  EntityAlreadyExistsError = Class.new(StandardError)
  EntityCreationError = Class.new(StandardError)

  rescue_from OnlyOneParameterAllowedError, with: :render_bad_request
  rescue_from EntityAlreadyExistsError, with: :render_conflict
  rescue_from EntityCreationError, with: :render_unprocessable_entity

  def error_json(msg)
    {
      errors: [
        msg
      ]
    }
  end

  private

  def render_bad_request
    render json: error_json('Bad request error'), status: :bad_request
  end

  def render_conflict
    render json: error_json('Record already exists'), status: :conflict
  end

  def render_unprocessable_entity
    render json: error_json('Entity creation error'), status: :unprocessable_entity
  end
end
