class FetcherWrapper
  def self.perform_request(resource_locator)
    outcome = LocationFetcher.fetch(resource_locator)

    outcome.to_h
  end
end
