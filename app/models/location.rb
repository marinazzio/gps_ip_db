class Location < ApplicationRecord
  has_many :aliases, class_name: 'LocationAlias', dependent: :destroy

  validates :ip, uniqueness: true, ip: true, unless: -> { ip.nil? }

  scope :success, -> { where(status: 'success') }
end
