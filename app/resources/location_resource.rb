class LocationResource < JSONAPI::Resource
  HIDE_FIELDS = %i[id created_at updated_at].freeze

  attributes :status, :ip, :latitude, :longitude, :aliases

  def aliases
    @model.aliases.map { AliasResource.new(_1, context) }
  end

  def fetchable_fields
    super - HIDE_FIELDS
  end
end
