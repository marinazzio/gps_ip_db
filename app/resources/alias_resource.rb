class AliasResource < JSONAPI::Resource
  HIDE_FIELDS = %i[id created_at updated_at].freeze

  model_name 'LocationAlias'

  attribute :name

  def fetchable_fields
    super - HIDE_FIELDS
  end
end
