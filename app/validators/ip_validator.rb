class IpValidator < ActiveModel::EachValidator
  class PrivateAddressError < StandardError; end
  class LoopbackAddressError < StandardError; end
  class LinkLocalAddressError < StandardError; end

  def validate_each(record, attribute, value)
    addr = IPAddr.new(value)
    raise PrivateAddressError if addr.private?
    raise LoopbackAddressError if addr.loopback?
    raise LinkLocalAddressError if addr.link_local?
  rescue PrivateAddressError, LoopbackAddressError, LinkLocalAddressError
    record.errors.add attribute, (options[:message] || 'should be public IP address')
  rescue StandardError
    record.errors.add attribute, (options[:message] || 'does not match IP address format')
  end
end
