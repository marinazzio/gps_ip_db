class FetchFromUplinkJob
  include Sidekiq::Job

  attr_reader :location, :response

  def perform(location_id)
    @location = Location.find(location_id)
    @response = FetcherWrapper.perform_request(resource_locator)

    update_fetching_status
    update_location_data

    location.save!
  end

  private

  def resource_locator
    location.ip.presence || location.aliases.first.name
  end

  def update_location_data
    return unless response[:status] == :success

    location.ip = response[:ip]
    location.longitude = response[:longitude]
    location.latitude = response[:latitude]
  end

  def update_fetching_status
    location.status = response[:message] || response[:status]
  end
end
